#include "skdaemonprocess.h"
#include "skdaemon.h"

ConstructorImpl(DaemonProcess, SkProcess)
{
    d = nullptr;
    executed = false;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool DaemonProcess::setup(Daemon *daemon)
{
    d = daemon;

    setProgram(d->program(), d->arguments(), d->environment(), d->workingDirectory());

    setMerged(PCH_STDIN, d->fifo(PCH_STDIN)->isMerged());
    setMerged(PCH_STDOUT, d->fifo(PCH_STDOUT)->isMerged());
    setMerged(PCH_STDERR, d->fifo(PCH_STDERR)->isMerged());

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool DaemonProcess::respawn()
{
    if (!executed)
    {
        ObjectError("Process respawning on NOT EXECUTED process");
        return false;
    }

    executed = false;
    execute();
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void DaemonProcess::onExecutorStart()
{
    if (!d->fifo(PCH_STDIN)->isMerged())
        d->fifo(PCH_STDIN)->setDevice(device(PCH_STDIN));

    if (!d->fifo(PCH_STDOUT)->isMerged())
        d->fifo(PCH_STDOUT)->setDevice(device(PCH_STDOUT));

    if (!d->fifo(PCH_STDERR)->isMerged())
        d->fifo(PCH_STDERR)->setDevice(device(PCH_STDERR));

    openFifos();
}

void DaemonProcess::onProcessFinished()
{
    executed = true;
}

void DaemonProcess::onExecutorFinished()
{
    if (!d->fifo(PCH_STDIN)->isMerged())
        d->fifo(PCH_STDIN)->unsetDevice();

    if (!d->fifo(PCH_STDOUT)->isMerged())
        d->fifo(PCH_STDOUT)->unsetDevice();

    if (!d->fifo(PCH_STDERR)->isMerged())
        d->fifo(PCH_STDERR)->unsetDevice();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool DaemonProcess::isExecuted()
{
    return executed;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

Daemon *DaemonProcess::daemon()
{
    return d;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

