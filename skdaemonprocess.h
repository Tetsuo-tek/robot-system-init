#ifndef SKDAEMONPROCESS_H
#define SKDAEMONPROCESS_H

#include <Core/System/IPC/skprocess.h>

class Daemon;

class DaemonProcess extends SkProcess
{
    bool executed;
    Daemon *d;

    public:
        Constructor(DaemonProcess, SkProcess);

        bool setup(Daemon *daemon);
        bool respawn();

        Daemon *daemon();
        bool isExecuted();

    private:
        void onExecutorStart()          override;
        void onProcessFinished()        override;
        void onExecutorFinished()       override;
};

#endif // SKDAEMONPROCESS_H
