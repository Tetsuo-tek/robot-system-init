#ifndef ROBOTSYSTEMINIT_H
#define ROBOTSYSTEMINIT_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "skdaemon.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class RobotSystemInit extends SkFlowSat
{
    SkString cfgFilePath;
    SkString daemonsDirectory;
    bool flowNetwork;
    bool exitOnIdle;
    bool quitting;

    SkStringList env;

    SkTreeMap<SkString, Daemon *> configuredDaemons;//name, cfg

    SkQueue<DaemonProcess *> initSequence;
    //SkStack<DaemonProcess *> shutDownSequence;
    SkTreeMap<SkString, DaemonProcess *> runningDaemons;//name, process

    public:
        Constructor(RobotSystemInit, SkFlowSat);

        Slot(onDaemonStart);
        Slot(onDaemonFinish);

    private:
        bool buildEnvironment();
        bool grabDaemonsCfg();
        bool buildInitQueue();

        bool onSetup()                                              override;
        void onInit()                                               override;
        void onCustomQuit()                                         override;

        void onFastTick()                                           override;
        void onSlowTick()                                           override;
        void onOneSecTick()                                         override;

        void onCheckInternals()                                     override;
        void onTickParamsChanged()                                  override;

        void onChannelAdded(SkFlowChanID chanID)                    override;
        void onChannelRemoved(SkFlowChanID chanID)                  override;
        void onChannelHeaderSetup(SkFlowChanID chanID)              override;

        void onFlowDataCome(SkFlowChannelData &chData)              override;

        void onChannelPublishStartRequest(SkFlowChanID chanID)      override;
        void onChannelPublishStopRequest(SkFlowChanID chanID)       override;

        void onChannelRedistrStartRequest(SkFlowChanID chanID)      override;
        void onChannelRedistrStopRequest(SkFlowChanID chanID)       override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // ROBOTSYSTEMINIT_H
