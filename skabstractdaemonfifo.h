#ifndef SKABSTRACTDAEMONFIFO_H
#define SKABSTRACTDAEMONFIFO_H

#include "Core/System/Network/FlowNetwork/skflowsat.h"
#include "Core/System/IPC/skprocess.h"

class AbstractDaemonFifo extends SkObject
{
    public:
        bool setup(SkTreeMap<SkString, SkVariant> &cfg, bool flowEnabled);

        void setDevice(SkFifo *fifo);
        void unsetDevice();

        SkProcessStream_T stdStreamType();
        bool isEnabled();
        bool isMerged();
        bool flowEnabled();

    protected:
        SkProcessStream_T t;
        bool merged;
        bool enabled;
        bool flowNetwork;
        SkFifo *device;

        AbstractConstructor(AbstractDaemonFifo, SkObject);

        virtual void onSetup()                      {}

        virtual void onSetDevice(SkFifo *)          {}
        virtual void onUnsetDevice()                {}

};

#endif // SKABSTRACTDAEMONFIFO_H
