#include "skabstractdaemonfifo.h"

AbstractConstructorImpl(AbstractDaemonFifo, SkObject)
{
    device = nullptr;

    t = PCH_NULL;

    enabled = false;
    merged = false;
    flowNetwork = false;
}

bool AbstractDaemonFifo::setup(SkTreeMap<SkString, SkVariant> &cfg, bool flowEnabled)
{
    enabled = cfg["enabled"].toBool();
    merged = cfg["merged"].toBool();
    flowNetwork = flowEnabled;

    onSetup();

    return true;
}

void AbstractDaemonFifo::setDevice(SkFifo *fifo)
{
    device = fifo;
    onSetDevice(device);
}

void AbstractDaemonFifo::unsetDevice()
{
    onUnsetDevice();
    device = nullptr;
}

SkProcessStream_T AbstractDaemonFifo::stdStreamType()
{
    return t;
}

bool AbstractDaemonFifo::isEnabled()
{
    return enabled;
}

bool AbstractDaemonFifo::isMerged()
{
    return merged;
}

bool AbstractDaemonFifo::flowEnabled()
{
    return flowNetwork;
}
