#ifndef DAEMONSTDIN_H
#define DAEMONSTDIN_H

#include "skabstractdaemonfifo.h"
#include "Core/System/Network/FlowNetwork/skflowgenericpublisher.h"
#include "Core/System/Network/FlowNetwork/skflowgenericsubscriber.h"

class DaemonStdIn extends AbstractDaemonFifo
{
    public:
        Constructor(DaemonStdIn, AbstractDaemonFifo);

        /*Slot(onPublisherReady);
        Slot(onDataAvailable);

    private:
        SkFlowGenericPublisher *fifoPublisher;//NOT PUBLISHING ANYTHING, ONLY MANAGE THE CHANNEL
        SkFlowGenericSubscriber *fifoSubscriber;

        void onSetup()                              override;
        void onSetDevice(SkFifo *fifo)              override;
        void onUnsetDevice()                        override;*/
};

#endif // DAEMONSTDIN_H
