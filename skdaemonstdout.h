#ifndef DAEMONSTDOUT_H
#define DAEMONSTDOUT_H

#include "skabstractdaemonfifo.h"
#include "Core/System/Network/FlowNetwork/skflowgenericpublisher.h"

class DaemonStdOut extends AbstractDaemonFifo
{
    public:
        Constructor(DaemonStdOut, AbstractDaemonFifo);

        /*Slot(onReadyRead);
        Signal(dataAvailable);

        SkAbstractFlowPublisher *publisher();

    protected:
        SkFlowGenericPublisher *fifoPublisher;

        void onSetup()                              override;
        void onSetDevice(SkFifo *fifo)              override;
        void onUnsetDevice()                        override;*/
};

#endif // DAEMONSTDOUT_H
