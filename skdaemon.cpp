#include "skdaemon.h"

#include <Core/System/skosenv.h>
#include "skdaemonstdin.h"
#include "skdaemonstderr.h"

ConstructorImpl(Daemon, SkObject)
{
    flowNetwork = false;
    instance = nullptr;
    respawn = false;
}

bool Daemon::setup(CStr *name, SkStringList &environment, SkTreeMap<SkString, SkVariant> &cfg, bool flowEnabled)
{
    n = name;
    flowNetwork = flowEnabled;

    prg = cfg["program"].toString();
    cfg["arguments"].copyToStringList(args);

    env = environment;

    workingDir = cfg["workingDirectory"].toString();
    depsOn = cfg["dependsOn"].toString();
    respawn =  cfg["respawn"].toBool();

    SkTreeMap<SkString, SkVariant> strCfg;
    cfg["stdin"].copyToMap(strCfg);

    fifos[PCH_STDIN] = new DaemonStdIn(this);
    fifos[PCH_STDIN]->setObjectName(this, "STDIN");

    if (!fifos[PCH_STDIN]->setup(strCfg, flowNetwork))
        return false;

    cfg["stdout"].copyToMap(strCfg);

    fifos[PCH_STDOUT] = new DaemonStdOut(this);
    fifos[PCH_STDOUT]->setObjectName(this, "STDOUT");

    if (!fifos[PCH_STDOUT]->setup(strCfg, flowNetwork))
        return false;

    cfg["stderr"].copyToMap(strCfg);

    fifos[PCH_STDERR] = new DaemonStdErr(this);
    fifos[PCH_STDERR]->setObjectName(this, "STDERR");

    if (!fifos[PCH_STDERR]->setup(strCfg, flowNetwork))
        return false;

    return true;
}

void Daemon::setProcess(DaemonProcess *process)
{
    instance = process;
}

void Daemon::unsetProcess()
{
    instance = nullptr;
}

void Daemon::addSequenceChild(DaemonProcess *child)
{
    childrenSequence.enqueue(child);
}

bool Daemon::hasSequenceChildren()
{
    return !childrenSequence.isEmpty();
}

DaemonProcess *Daemon::dequeueSequenceChild()
{
    if (!hasSequenceChildren())
        return nullptr;

    return childrenSequence.dequeue();
}

CStr *Daemon::name()
{
    return n.c_str();
}

CStr *Daemon::program()
{
    return prg.c_str();
}

SkStringList &Daemon::arguments()
{
    return args;
}

SkStringList &Daemon::environment()
{
    return env;
}

CStr *Daemon::workingDirectory()
{
    return workingDir.c_str();
}

bool Daemon::depends()
{
    return !depsOn.isEmpty();
}

CStr *Daemon::dependsOnName()
{
    return depsOn.c_str();
}

bool Daemon::hasRespawn()
{
    return respawn;
}

AbstractDaemonFifo *Daemon::fifo(SkProcessStream_T t)
{
    return fifos[t];
}

DaemonProcess *Daemon::process()
{
    return instance;
}

bool Daemon::flowEnabled()
{
    return flowNetwork;
}
