﻿#include "robotsysteminit.h"
#include <Core/System/Filesystem/skfsutils.h>
#include <Core/System/Filesystem/skfileinfoslist.h>

#include "skdaemonstderr.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(RobotSystemInit, SkFlowSat)
{
    flowNetwork = false;
    quitting = false;
    exitOnIdle = false;

    setObjectName("RobotSystemInit");

    disableConnectionOnStart();
    disableAutomaticQuit();

    SlotSet(onDaemonStart);
    SlotSet(onDaemonFinish);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool RobotSystemInit::onSetup()
{
    SkCli *cli = skApp->appCli();

    cfgFilePath = cli->value("--config").toString();

    SkVariant cfgVal;
    AssertKiller(!SkFsUtils::readJSON(cfgFilePath.c_str(), cfgVal));

    SkTreeMap<SkString, SkVariant> cfg;
    cfgVal.copyToMap(cfg);

    SkString workingDir = cfg["workingDirectory"].toString();

    if (!workingDir.isEmpty())
        setWorkingDir(workingDir.c_str());

    daemonsDirectory = cfg["daemonsDirectory"].toString();
    AssertKiller(daemonsDirectory.isEmpty());

    ObjectMessage("Setup daemons configuration directory: " << daemonsDirectory);

    exitOnIdle = cfg["exitOnIdle"].toBool();
    ObjectMessage("This program will exit on IDLE state (no daemons running): " << SkVariant::boolToString(exitOnIdle));

    flowNetwork =  cfg["flowNetwork"].toBool();

    AssertKiller(!buildEnvironment());
    AssertKiller(!grabDaemonsCfg());
    AssertKiller(!buildInitQueue());

    ObjectMessage("Setup completed");
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool RobotSystemInit::buildEnvironment()
{
    ObjectMessage("Grabbing environment variables from OS ..");

    SkTreeMap<SkString, SkString> &environment = skApp->osEnv()->getEnvironment();
    SkBinaryTreeVisit<SkString, SkString> *itr = environment.iterator();

    while(itr->next())
    {
        env << "";
        Stringify(env.last(), itr->item().key() << "=" << itr->item().value());
        ObjectMessage("ENV-PAIR -> " << env.last());
    }

    delete itr;
    return true;
}

bool RobotSystemInit::grabDaemonsCfg()
{
    ObjectMessage("Grabbing daemons configurations ..");

    SkFileInfosList *fileInfos = new SkFileInfosList(this);
    fileInfos->setObjectName(this, "DaemonsList");

    if (!SkFsUtils::ls(daemonsDirectory.c_str(), fileInfos))
    {
        fileInfos->destroyLater();
        return false;
    }

    if (exitOnIdle && fileInfos->isEmpty())
    {
        ObjectError("There are NOT daemons to start [exitOnIdle=true]");
        fileInfos->destroyLater();
        return false;
    }

    fileInfos->open();

    while(fileInfos->next())
    {
        SkVariant daemonVal;

        if (!SkFsUtils::readJSON(fileInfos->currentPath().c_str(), daemonVal))
            continue;

        SkTreeMap<SkString, SkVariant> daemonCfg;
        daemonVal.copyToMap(daemonCfg);

        SkPathInfo info;
        SkFsUtils::fillPathInfo(fileInfos->currentPath().c_str(), info);

        if (!daemonCfg["enabled"].toBool())
        {
            ObjectWarning("Daemon is DISABLED by configuration: " << info.name);
            continue;
        }

        Daemon *d = new Daemon(this);
        d->setObjectName(this, info.name.c_str());

        if (!d->setup(info.name.c_str(), env, daemonCfg, flowNetwork))
        {
            d->destroyLater();
            continue;
        }

        if (flowNetwork)
        {
            /*setupPublisher(DynCast(DaemonStdOut, d->fifo(PCH_STDOUT))->publisher());
            setupPublisher(DynCast(DaemonStdErr, d->fifo(PCH_STDERR))->publisher());*/
        }

        configuredDaemons[d->name()] = d;
        ObjectMessage("CHECKED daemon config: " << d->name());
    }

    fileInfos->close();
    fileInfos->destroyLater();

    return true;
}

bool RobotSystemInit::buildInitQueue()
{
    if (configuredDaemons.isEmpty())
        return true;

    ObjectMessage("Setup INIT-QUEQUE ..");

    SkBinaryTreeVisit<SkString, Daemon *> *itr = configuredDaemons.iterator();

    while(itr->next())
    {
        Daemon *d = itr->item().value();

        DaemonProcess *p = new DaemonProcess(this);
        p->setObjectName(this, d->name());

        if (!p->setup(d))
        {
            p->destroyLater();
            continue;
        }

        if (d->depends())
        {
            if (SkString::compare(d->name(), d->dependsOnName()))
            {
                ObjectError("DEPENDENCE '" << d->dependsOnName() << "' is NOT valid (has the same name of the parent); CANNOT start daemon: " << d->name());
                p->destroyLater();
                continue;
            }

            if (!configuredDaemons.contains(d->dependsOnName()))
            {
                ObjectError("DEPENDENCE '" << d->dependsOnName() << "' is NOT valid (not found); CANNOT start daemon: " << d->name());
                p->destroyLater();
                continue;
            }

            Daemon *target = configuredDaemons[d->dependsOnName()];
            target->childrenSequence.enqueue(p);

            ObjectMessage("DEPENDING daemon ADDED to '" << target->name() << "' sublaunch-sequence: " << d->name());
        }

        else
        {
            initSequence.enqueue(p);
            ObjectMessage("INIDIPENDENT daemon ADDED to global launch-sequence: " << d->name());
        }

        Attach(p, started, this, onDaemonStart, SkDirect);
        Attach(p, finished, this, onDaemonFinish, SkQueued);

        d->setProcess(p);
    }

    delete itr;
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotSystemInit::onInit()
{
    if (initSequence.isEmpty())
    {
        ObjectMessage("Ready");
        return;
    }

    initSequence.dequeue()->execute();
}

void RobotSystemInit::onCustomQuit()
{
    ObjectMessage("Shutdown ..");
    quitting = true;

    if (runningDaemons.isEmpty())
    {
        eventLoop()->invokeSlot(quit_SLOT, this, this);
        return;
    }

    SkBinaryTreeVisit<SkString, DaemonProcess *> *itr = runningDaemons.iterator();

    while(itr->next())
    {
        DaemonProcess *p = itr->item().value();

        if (!itr->hasNext())
        {
            exitOnIdle = true;
            Attach(p, finished, this, quit, SkQueued);
        }

        p->terminate();
        //p->kill();
        p->release();

    }

    delete itr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotSystemInit, onDaemonStart)
{
    SilentSlotArgsWarning();

    DaemonProcess *next = nullptr;

    DaemonProcess *p = DynCast(DaemonProcess, referer);
    Daemon *d = p->daemon();

    runningDaemons[d->name()] = p;

    if (!initSequence.isEmpty())
        next = initSequence.dequeue();

    while(d->hasSequenceChildren())
        initSequence.enqueue(d->dequeueSequenceChild());

    if (!next && !initSequence.isEmpty())
        next = initSequence.dequeue();

    if (next)
        next->execute();
}

SlotImpl(RobotSystemInit, onDaemonFinish)
{
    SilentSlotArgsWarning();

    DaemonProcess *p = DynCast(DaemonProcess, referer);
    Daemon *d = p->daemon();

    if (!skApp->isSignaledToQuit() && !quitting && d->hasRespawn())
    {
        ObjectWarning("RESPAWNING daemon: " << d->name());
        p->removeProperty("DIED");

        if (p->respawn())
            return;
    }

    d->unsetProcess();

    runningDaemons.remove(d->name());
    p->destroyLater();

    ObjectWarning("REMOVED process-instance for EXECUTED daemon: " << d->name());

    if (runningDaemons.isEmpty())
    {
        ObjectWarning("There are NOT running daemons");

        if (!skApp->isSignaledToQuit() && exitOnIdle)
        {
            ObjectWarning("Exiting on IDLE status");
            onCustomQuit();
        }
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotSystemInit::onFastTick()
{}

void RobotSystemInit::onSlowTick()
{}

void RobotSystemInit::onOneSecTick()
{
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotSystemInit::onCheckInternals()
{
    SkBinaryTreeVisit<SkString, DaemonProcess *> *itr = runningDaemons.iterator();

    while(itr->next())
    {
        DaemonProcess *p = itr->item().value();

        if (p->existsProperty("DIED"))
            p->release();

        else if (p->isExecuted())
            p->setProperty("DIED", SkVariant());
    }

    delete itr;

    if (flowNetwork && !isConnected())
        tryToConnect();
}

void RobotSystemInit::onTickParamsChanged()
{}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotSystemInit::onChannelAdded(SkFlowChanID chanID)
{}

void RobotSystemInit::onChannelRemoved(SkFlowChanID chanID)
{}

void RobotSystemInit::onChannelHeaderSetup(SkFlowChanID chanID)
{}

void RobotSystemInit::onFlowDataCome(SkFlowChannelData &chData)
{}

void RobotSystemInit::onChannelPublishStartRequest(SkFlowChanID chanID)
{}

void RobotSystemInit::onChannelPublishStopRequest(SkFlowChanID chanID)
{}

void RobotSystemInit::onChannelRedistrStartRequest(SkFlowChanID chanID)
{}

void RobotSystemInit::onChannelRedistrStopRequest(SkFlowChanID chanID)
{}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
