#ifndef SKDAEMON_H
#define SKDAEMON_H

#include "skdaemonprocess.h"
#include "skabstractdaemonfifo.h"

struct Daemon extends SkObject
{
    SkString n;
    bool flowNetwork;
    bool respawn;

    SkString prg;
    SkStringList args;
    SkStringList env;
    SkString workingDir;

    SkString depsOn;
    SkQueue<DaemonProcess *> childrenSequence;

    AbstractDaemonFifo *fifos[3];

    DaemonProcess *instance;

    public:
        Constructor(Daemon, SkObject);
        bool setup(CStr *name, SkStringList &environment, SkTreeMap<SkString, SkVariant> &cfg, bool flowEnabled);

        void setProcess(DaemonProcess *process);
        void unsetProcess();

        void addSequenceChild(DaemonProcess *child);
        bool hasSequenceChildren();
        DaemonProcess *dequeueSequenceChild();

        CStr *name();
        CStr *program();
        SkStringList &arguments();
        SkStringList &environment();
        CStr *workingDirectory();
        bool depends();
        CStr *dependsOnName();
        bool hasRespawn();

        AbstractDaemonFifo *fifo(SkProcessStream_T t);
        DaemonProcess *process();

        bool flowEnabled();
};

#endif // SKDAEMON_H
