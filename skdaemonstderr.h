#ifndef DAEMONSTDERR_H
#define DAEMONSTDERR_H

#include "skdaemonstdout.h"

class DaemonStdErr extends DaemonStdOut
{
    public:
        Constructor(DaemonStdErr, DaemonStdOut);
};

#endif // DAEMONSTDERR_H
