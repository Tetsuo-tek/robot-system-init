#include "robotsysteminit.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);

    logger->enablePID(true);
    logger->enableDateTime(true);

    SkCli *cli = skApp->appCli();
    cli->add("--config", "-i", "config.json", "Select init configuration json file path");

#if defined(ENABLE_HTTP)
    SkFlowSat::addHttpCLI();
#endif

    skApp->init(5000, 150000, SK_TIMEDLOOP_RT);
    new RobotSystemInit;

    return skApp->exec();
}
