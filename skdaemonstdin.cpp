#include "skdaemonstdin.h"

ConstructorImpl(DaemonStdIn, AbstractDaemonFifo)
{
    //fifoSubscriber = nullptr;

    t = PCH_STDIN;

    /*SlotSet(onPublisherReady);
    SlotSet(onDataAvailable);*/
}

/*void DaemonStdIn::onSetup()
{
    if (flowNetwork)
    {
        fifoPublisher = new SkFlowGenericPublisher(this);
        Attach(fifoPublisher, ready, this, onPublisherReady, SkDirect);

        fifoSubscriber = new SkFlowGenericSubscriber(this);
        Attach(fifoSubscriber, dataAvailable, this, onDataAvailable, SkDirect);
    }
}

void DaemonStdIn::onSetDevice(SkFifo *fifo)
{
    if (flowNetwork)
    {
        fifoPublisher->setObjectName(this, "FakePublisher");
        fifoPublisher->setup(objectName(), FT_BLOB, T_BYTEARRAY, "", nullptr);
        fifoPublisher->start();

        fifoSubscriber->setObjectName(this, "Subsriber");
    }
}

void DaemonStdIn::onUnsetDevice()
{
    if (flowNetwork)
    {
        fifoSubscriber->close();
        fifoPublisher->stop();
    }
}

SlotImpl(DaemonStdIn, onPublisherReady)
{
    SilentSlotArgsWarning();
    fifoSubscriber->subscribe(fifoPublisher->getChannel()->name.c_str());
}

SlotImpl(DaemonStdIn, onDataAvailable)
{
    SilentSlotArgsWarning();
    device->write(&data);
}*/
