#!/bin/bash

# chmod +x execute-process.sh
# ./execute-process.sh "[<path>]<programma> <arg1> <arg2> ... <argN>" <prog_name

# CHECK ARGS
if [ "$#" -ne 2 ]; then
    echo "Usage: ./execute-process.sh  <cli> <infoDir>"
    exit 1
fi

process_dir=/tmp/$2
rm -rf $stdin_fifo
mkdir -v $process_dir

stdin_fifo=$process_dir/stdin.FIFO
stdout_fifo=$process_dir/stdout.FIFO
stderr_fifo=$process_dir/stderr.FIFO

rm -f $stdin_fifo
rm -f $stdout_fifo
rm -f $stderr_fifo

mkfifo $stdin_fifo
mkfifo $stdout_fifo
mkfifo $stderr_fifo

#$1 < $stdin_fifo 1> $stdout_fifo 2> $stderr_fifo &
$1 1> $stdout_fifo 2> $stderr_fifo

echo "$pid" > $process_dir/PID
echo "Program LAUNCHED, with PID: $pid"

